import java.util.Random;

public class OnMine extends State {
	
	private static OnMine OnlyOne=null;
	Random gener=new Random();
	private OnMine()
	{
		
	}
	
	public static  OnMine getInstance()
	{
		if(OnlyOne==null)
		{
			OnlyOne=new OnMine();
		}
		return OnlyOne;
	}
	

	@Override
	public void exec(Enity body) {
		// TODO Auto-generated method stub
		
		if(body instanceof Man)
		{
			Man toOperate=(Man) body;
			if(toOperate.getState()!=OnMine.getInstance())
			{
				System.out.println("The man walk to mine");
				toOperate.changeState(OnMine.getInstance());
				
			}
			if(toOperate.getCurrentPower()==0)
			{
				System.out.println("man's pow is too low,he should sleep");
				toOperate.changeState(Tired.getInstance());
				return;
			}
			//work to get gold has 1/3 property to get the gold
			if(gener.nextInt(3)==2)
			{
				int getGold=gener.nextInt(toOperate.getMaxCapacity());
				if(toOperate.getCurrentCapacity()+getGold>toOperate.getMaxCapacity())
				{
					System.out.println("Gread have find gold but too much("+getGold+"), man should go to store");
					toOperate.addToCurrentCapacity(toOperate.getMaxCapacity()-toOperate.getCurrentCapacity());
					
				}
				else
				{
					
					toOperate.addToCurrentCapacity(getGold);
					System.out.println("Man get the gold "+getGold+"there captity is "+toOperate.getCurrentCapacity());
				}
				
				
			}
			else
			{
				System.out.println("man don't have good luck,nothing get");
			}
			toOperate.decreasePower();
			
			System.out.println("man have power:"+toOperate.getCurrentPower());
			if(toOperate.getCurrentPower()==0)
			{
				System.out.println("man's pow is too low,he should sleep");
				toOperate.changeState(Tired.getInstance());
				return;
			}
			if(toOperate.getCurrentCapacity()==toOperate.getMaxCapacity())
			{
				System.out.println("man's bag if full and he should go to change it");
				toOperate.changeState(BagFull.getInstance());
			}
			
		}
		
	}

}
