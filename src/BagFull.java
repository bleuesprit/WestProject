import java.util.Scanner;

public class BagFull extends State {

	private static BagFull OnlyOne=null;
	
	private BagFull()
	{
		
	}
	

	public static BagFull getInstance()
	{
		if(OnlyOne==null)
		{
			OnlyOne=new BagFull();
		}
		return OnlyOne;
	}
	@Override
	public void exec(Enity body) {
		// TODO Auto-generated method stub
		if(body instanceof Man)
		{
			Man toOperate=(Man) body;
			if(toOperate.getCurrentCapacity()!=toOperate.getMaxCapacity())
			{
				System.out.println("error in bagfull beacause bag is not full");
				new Scanner(System.in).nextLine();//system("pause")
			}
			
			System.out.println("man go to bank put the gold for money and he is very happy");	
			toOperate.addMoney(toOperate.getCurrentCapacity()*10);
			System.out.println("man have money"+toOperate.getMoney());
			toOperate.setCurrentCapacity(0);
			
			toOperate.decreasePower();

			
			
			if(toOperate.getState()!=OnMine.getInstance())
			{
				System.out.println("man should move to the mine");
				toOperate.changeState(OnMine.getInstance());
			}
		}
		
	}

	
}
