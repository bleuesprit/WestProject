import java.util.Scanner;

public class Tired extends State {

	
	private static Tired OnlyOne=null;
	private Tired()
	{
		
	}
	
	public static Tired getInstance()
	{
		if(OnlyOne==null)
		{
			OnlyOne=new Tired();
		}
		return OnlyOne;
	}
	
	@Override
	public void exec(Enity body) {
		// TODO Auto-generated method stub
		if(body instanceof Man)
		{
			Man toOperate=(Man) body;
			if(toOperate.getCurrentPower()!=0)
			{
				System.out.println("there is some problem in Tired currentpow is not 0");
				new Scanner(System.in).nextLine();//system("pause")		
			}
			System.out.println("man is tired and should sleep a while");
//			try
//			{
//				Thread.sleep(1000*10);//sleep 10s;
//			}
//			catch(InterruptedException e)
//			{
//				e.printStackTrace();
//			}
			toOperate.recorverPower();
			if(toOperate.getCurrentCapacity()==toOperate.getMaxCapacity())
			{
				System.out.println("man find his bag is full after sleep,he should go to store it ");
				toOperate.changeState(BagFull.getInstance());
			}
			else
			{
				System.out.println("man is ready .and go to mine");
				toOperate.changeState(OnMine.getInstance());
			}
		}
	}
	

}
