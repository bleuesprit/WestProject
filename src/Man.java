
public class Man extends Enity {
	private int maxCapacity;
	private int currentCapacity;
	private int maxPower;
	private int currentPower;
	private int money;
	private State preState;
	private State state;
	
	
	public Man()
	{
		maxCapacity=100;
		currentCapacity=0;
		maxPower=10;
		currentPower=maxPower;
		money=0;
		preState=null;
		state=OnMine.getInstance();
	}
	public void update()
	{
		state.exec(this);
		
	}
	public void changeState(State toState)
	{
		preState=state;
		state=toState;
	}
	
	public State getState()
	{
		return state;
	}
	
	public int getCurrentCapacity()
	{
		return currentCapacity;
	}
	public int getMaxCapacity()
	{
		return maxCapacity;
	}
	
	public void addToCurrentCapacity(int src)
	{
		if(src<0||currentCapacity+src>maxCapacity)
		{
			System.out.println("addtoCurrentcapacity error src negative or capacity overflow");
		}
		this.currentCapacity+=src;
	}
	
	public void setCurrentCapacity(int src)
	{
		currentCapacity=src;
	}
	
	public void decreasePower()
	{
		this.currentPower--;
		if(currentPower==0)
		{
			changeState(Tired.getInstance());
		}
		
	}
	
	public int getMoney()
	{
		return money;
	}
	
	public void addMoney(int src)
	{
		money+=src;
	}
	
	public void setMoney(int src)
	{
		money=src;
	}
	
	public int getCurrentPower()
	{
		return currentPower;
	}
	
	
	public void recorverPower()
	{
		currentPower=maxPower;
	}
}
